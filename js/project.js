$(document).ready(function() {

	var jsonPath = "data/students.json";

	$.ajax({
		url : jsonPath,
		dataType : "json",
		success : parseData,
		error : function(result) {
			console.log("Error loading data");
		}
	});

	function parseData(data) {

		$(data).each(function(k, v) {

			var student = data[k].student,
				english = data[k].subjects.english,
				science = data[k].subjects.science,
				history = data[k].subjects.history,
				math 	= data[k].subjects.math,
				physicalEd = data[k].subjects.physicalEd;
			
			var eNotes = data[k].notes.english,
				sNotes = data[k].notes.science,
				hNotes = data[k].notes.history,
				mNotes = data[k].notes.math,
				pNotes = data[k].notes.physicalEd;

			var gpa = [english, science, history, math, physicalEd];

			var sum = (eval(gpa.join("+")) / 5).toFixed(2);
			var perc = (sum / 4.0) * 100;

			var engPer = (english / 4.0) * 100;
			var sciPer = (science / 4.0) * 100;
			var hisPer = (history / 4.0) * 100;
			var mathPer = (math / 4.0) * 100;
			var pePer = (physicalEd / 4.0) * 100;

			
			var overallGPA = "<div class='gpa'>GPA: <span>" + sum + "</span></div>";
			var engCh = "<li title='English'><span class='bar' data-toggle='tooltip' data-placement='bottom' title='"+eNotes+"' data-number='" + engPer + "'/><span class='number'>" + english + "</span></li>";
			var sciCh = "<li title='Science'><span class='bar' data-toggle='tooltip' data-placement='bottom' title='"+sNotes+"' data-number='" + sciPer + "'/><span class='number'>" + science + "</span></li>";
			var hisCh = "<li title='History'><span class='bar' data-toggle='tooltip' data-placement='bottom' title='"+hNotes+"' data-number='" + hisPer + "'/><span class='number'>" + history + "</span></li>";
			var mathCh = "<li title='Math'><span class='bar' data-toggle='tooltip' data-placement='bottom' title='"+mNotes+"' data-number='" + mathPer + "'/><span class='number'>" + math + "</span></li>";
			var phEdCh = "<li title='Phys. Ed'><span class='bar' data-toggle='tooltip' data-placement='bottom' title='"+pNotes+"' data-number='" + pePer + "'/><span class='number'>" + physicalEd + "</span></li>";

			var chart = "<ul class='chart'>" + engCh + sciCh + hisCh + mathCh + phEdCh + "</ul>";
			var gradeDiv = "<div class='grades'>" + chart + "</div>";
			var studentDiv = "<div class='student' id=" + student + ">" + gradeDiv + overallGPA + "</div>";

			$("#main").append(studentDiv);

			var divs = d3.select("#" + student);
			var rp = radialProgress("#" + student).label(student).diameter(150).value(perc).render();

		});
		/* Tool tip for notes*/
		$('[data-toggle="tooltip"]').tooltip();
		
		/* Coloring of radial progress */
		$('.student').each(function() {
			var val = $(this).find('.gpa span').html();
			var perc = ((val / 4) * 100).toFixed();

			switch(true) {
			case (perc >= 90):
				$(this).addClass('a');
				break;
			case (perc >= 80 && perc <= 89):
				$(this).addClass('b');
				break;
			case (perc >= 70 && perc <= 79):
				$(this).addClass('c');
				break;
			case (perc >= 64 && perc <= 69):
				$(this).addClass('d');
				break;
			default:
				$(this).addClass('f');
			}
		});
		/* Coloring of bar graph */
		$('.chart li').each(function() {
			var val = $(this).find('.number').html();
			var perc = (val / 4) * 100;

			switch(true) {
			case (perc >= 90):
				$(this).addClass('a');
				break;
			case (perc >= 80 && perc <= 89):
				$(this).addClass('b');
				break;
			case (perc >= 70 && perc <= 79):
				$(this).addClass('c');
				break;
			case (perc >= 64 && perc <= 69):
				$(this).addClass('d');
				break;
			default:
				$(this).addClass('f');
			}
		});

		/* Interactive*/
		$('.student').click(function() {
			var $elemId = $(this).attr("id");

			deselect();
			$(this).addClass("selectedRadial");
			$(this).animate({
				width : '450px'
			}, function() {
				var selector = $('.bar');
				var highestNumber = 100;

				$(this).find($(selector)).each(function() {
					var bar = $(this), num = bar.data('number'), percentage = Math.round((num / highestNumber) * 100) + '%';

					$(this).animate({
						'width' : percentage
					}, 1000);
					$(this).next('.number').animate({
						'left' : percentage
					}, 1000);
				});
			});
			$(this).find('.grades').fadeIn();
		});

		function deselect() {

			$('.student').removeClass('selectedRadial').css({
				'width' : '200px'
			});
			$('.grades').hide();
			$('.number').css({
				'left' : '0'
			});

			$('.chart .bar').css({
				'width' : '0'
			});
		}

	}

});
